package com.fernapp.useragent.swing;

import java.io.IOException;
import java.net.UnknownHostException;

import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.useragent.ApplicationSessionConnectionDetails;
import com.fernapp.useragent.DefaultConnectionKeeper;
import com.fernapp.useragent.RaupClient;
import com.fernapp.useragent.ServiceHub;
import com.fernapp.util.Callback;



/**
 * @author Markus
 * 
 */
public class StandaloneUserAgent {

	public static void main(String[] args) throws UnknownHostException, IOException {
		Thread.currentThread().setName("main");

		// ApplicationSessionConnectionDetails
		if (args.length != 2) {
			throw new IllegalArgumentException("Specify host and port");
		}
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		ApplicationSessionConnectionDetails connectionDetails = new ApplicationSessionConnectionDetails(host, port);

		// RAUP client
		final RaupClient raupClient = new SwingStandaloneRaupClient();

		// connectionKeeper
		ServiceHub.connectionKeeper = new DefaultConnectionKeeper();
		ServiceHub.connectionKeeper.addConnectionChangeListener(new Callback<MessagingConnection>() {
			public void onCallback(MessagingConnection mc) {
				raupClient.initForConnection(mc);
			}
		});
		ServiceHub.connectionKeeper.keepConnection(connectionDetails);
	}

}
