package com.fernapp.useragent.swing.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JPanel;

import com.fernapp.useragent.ui.InputEventListener;
import com.fernapp.useragent.ui.WindowContainer;
import com.fernapp.useragent.ui.WindowStructuralListener;


/**
 * An abstract {@link WindowContainer} wrapper of an AWT {@link Container} to display a
 * remote window.
 * @author Markus
 */
public abstract class AbstractAwtContainerWrapper implements WindowContainer {

	protected final Container container;
	// TODO do we really need another encapsulation??
	private final JPanel contentPanel;
	private volatile BufferedImage bufferedImage;
	private volatile Point contentLocation;
	private final AtomicBoolean nextResizeCausedBySoftware = new AtomicBoolean(false);
	protected final List<WindowStructuralListener> structuralListeners = new CopyOnWriteArrayList<WindowStructuralListener>();

	protected AbstractAwtContainerWrapper(Container container) {
		this.container = container;
		this.contentPanel = new ContentPanel();
		container.setLayout(new BorderLayout());
		container.add(contentPanel, BorderLayout.CENTER);
		container.setFocusTraversalKeysEnabled(false);
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#addInputEventListener(com.fernapp.useragent.ui.InputEventListener)
	 */
	public final void addInputEventListener(final InputEventListener inputEventListener) {
		container.setFocusable(true);
		KeyListener keyListener = new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				inputEventListener.keyPressed(e);
			}

			public void keyReleased(KeyEvent e) {
				inputEventListener.keyReleased(e);
			}
		};
		container.addKeyListener(keyListener);

		contentPanel.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				inputEventListener.mousePressed(e.getButton());
				container.requestFocus();
			}

			public void mouseReleased(MouseEvent e) {
				inputEventListener.mouseReleased(e.getButton());
			}
		});

		contentPanel.addMouseMotionListener(new MouseAdapter() {
			public void mouseMoved(MouseEvent e) {
				inputEventListener.mouseMoved(e.getX(), e.getY());
			}

			public void mouseDragged(MouseEvent e) {
				inputEventListener.mouseMoved(e.getX(), e.getY());
			}
		});
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#addWindowStructuralListener(com.fernapp.useragent.ui.WindowStructuralListener)
	 */
	public void addWindowStructuralListener(final WindowStructuralListener listener) {
		structuralListeners.add(listener);

		contentPanel.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				boolean softwareCause = nextResizeCausedBySoftware.getAndSet(false);
				listener.resized(e.getComponent().getWidth(), e.getComponent().getHeight(), !softwareCause);
			}
		});

		addWindowCloseListener(listener);
	}

	public abstract void addWindowCloseListener(final WindowStructuralListener listener);

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#getContentWidth()
	 */
	public int getContentWidth() {
		return contentPanel.getWidth();
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#getContentHeight()
	 */
	public int getContentHeight() {
		return contentPanel.getHeight();
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#setContentLocation(int, int)
	 */
	public void setContentLocation(int left, int top) {
		contentLocation = new Point(left, top);
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#getContentLocationLeft()
	 */
	public int getContentLocationLeft() {
		Point p;
		if (contentPanel.isDisplayable()) {
			p = contentPanel.getLocationOnScreen();
		} else if (contentLocation != null) {
			p = contentLocation;
		} else {
			throw new IllegalStateException("Location unknown");
		}
		return p.x;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#getContentLocationTop()
	 */
	public int getContentLocationTop() {
		Point p;
		if (contentPanel.isDisplayable()) {
			p = contentPanel.getLocationOnScreen();
		} else if (contentLocation != null) {
			p = contentLocation;
		} else {
			throw new IllegalStateException("Location unknown");
		}
		return p.y;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#updateContent(java.awt.image.BufferedImage)
	 */
	public void updateContent(BufferedImage bi) {
		this.bufferedImage = bi;
		contentPanel.repaint();
	}

	public class ContentPanel extends JPanel {
		public void paintComponent(Graphics g) {
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getContentWidth(), getContentHeight());

			if (bufferedImage != null) {
				g.drawImage(bufferedImage, 0, 0, null);
			}
		}
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#show()
	 */
	public void show() {
		container.setVisible(true);
	}

	/**
	 * @return
	 * @see java.awt.Component#isVisible()
	 */
	public boolean isVisible() {
		return container.isVisible();
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#setContentSize(int, int)
	 */
	public final void setContentSize(int width, int height) {
		nextResizeCausedBySoftware.set(true);
		contentPanel.setPreferredSize(new Dimension(width, height));
		adjustContainerSizeToContentPanel();
	}

	protected abstract void adjustContainerSizeToContentPanel();
	
	public Container getAwtContainer() {
		return container;
	}

}
