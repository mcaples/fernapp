package com.fernapp.useragent.swing.ui;

import javax.swing.JFrame;
import javax.swing.WindowConstants;


/**
 * Implementation for remote windows of type "normal".
 * @author Markus
 */
public class DecoratedRemoteWindow extends AbstractAwtWindowWrapper {

	private final JFrame frame;

	public DecoratedRemoteWindow() {
		super(new JFrame());
		frame = (JFrame) getWindow();
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	}

	/**
	 * @see com.fernapp.useragent.swing.ui.UndecoratedRemoteWindow#setTitle(java.lang.String)
	 */
	public void setTitle(String title) {
		frame.setTitle(title);
	}

}
