package com.fernapp.useragent.swing.ui;

import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.fernapp.useragent.ui.WindowContainer;
import com.fernapp.useragent.ui.WindowStructuralListener;


/**
 * An abstract {@link WindowContainer} wrapper of an AWT {@link Window}.
 * @author Markus
 */
public abstract class AbstractAwtWindowWrapper extends AbstractAwtContainerWrapper {

	private final Window window;

	protected AbstractAwtWindowWrapper(Window window) {
		super(window);
		this.window = window;
	}

	/**
	 * @see com.fernapp.useragent.swing.ui.AbstractAwtContainerWrapper#addWindowCloseListener(com.fernapp.useragent.ui.WindowStructuralListener)
	 */
	@Override
	public void addWindowCloseListener(final WindowStructuralListener listener) {
		window.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				listener.closing();
			}
		});
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#setContentLocation(int, int)
	 */
	public void setContentLocation(int left, int top) {
		super.setContentLocation(left, top);
		window.pack();
		window.setLocation(left - window.getInsets().left, top - window.getInsets().top);
	}

	/**
	 * @see com.fernapp.useragent.swing.ui.AbstractAwtContainerWrapper#adjustContainerSizeToContentPanel()
	 */
	@Override
	protected void adjustContainerSizeToContentPanel() {
		window.pack();
	}

	/**
	 * 
	 * @see java.awt.Window#dispose()
	 */
	public void dispose() {
		window.dispose();
	}

	/**
	 * @return the container
	 */
	public Window getWindow() {
		return window;
	}

}
