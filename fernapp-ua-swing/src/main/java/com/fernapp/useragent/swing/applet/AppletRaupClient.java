package com.fernapp.useragent.swing.applet;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JOptionPane;

import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.useragent.AbstractRaupClient;
import com.fernapp.useragent.RaupClient;
import com.fernapp.useragent.swing.applet.AppletUserAgent.AppletRemoteWindowContainer;
import com.fernapp.useragent.swing.ui.DecoratedRemoteWindow;
import com.fernapp.useragent.swing.ui.UndecoratedRemoteWindow;
import com.fernapp.useragent.ui.RemoteWindowController;
import com.fernapp.useragent.ui.WindowContainer;


/**
 * {@link RaupClient} implementation for a swing standalone user agent.
 * @author Markus
 */
public class AppletRaupClient extends AbstractRaupClient {

	private final AppletRemoteWindowContainer appletWindowContainer;
	private boolean first = true;
	private Dimension browserWindowSize;
	private RemoteWindowController controllerToAdjustSize;

	public AppletRaupClient(AppletRemoteWindowContainer appletWindowContainer) {
		this.appletWindowContainer = appletWindowContainer;
	}

	/**
	 * @see com.fernapp.useragent.AbstractRaupClient#createRemoteWindowController(com.fernapp.raup.windowmanagement.server.WindowSettings)
	 */
	@Override
	protected RemoteWindowController createRemoteWindowController(WindowSettings windowSettings) {
		WindowContainer windowContainer;

		boolean browserContainedWindow = first;
		first = false;

		if (browserContainedWindow) {
			windowContainer = appletWindowContainer;
		} else {
			if (windowSettings.getParentWindowId() == null) {
				windowContainer = new DecoratedRemoteWindow();
			} else {
				windowContainer = new UndecoratedRemoteWindow();
			}
		}

		RemoteWindowController remoteWindowController = new RemoteWindowController(windowSettings.getWindowId(), windowContainer, this);
		if (browserContainedWindow) {
			synchronized (this) {
				if (browserWindowSize != null) {
					remoteWindowController.requestResize(browserWindowSize.width, browserWindowSize.height);
				} else {
					// browser size not yet known -> resize when setter is called
					controllerToAdjustSize = remoteWindowController;

				}
			}
		}

		return remoteWindowController;
	}

	/**
	 * @see com.fernapp.useragent.AbstractRaupClient#showMessage(java.lang.String)
	 */
	@Override
	protected void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	/**
	 * @see com.fernapp.useragent.AbstractRaupClient#getPassphrase()
	 */
	@Override
	protected String getPassphrase() {
		return JOptionPane.showInputDialog(appletWindowContainer.getAwtContainer(), "Enter passphrase:");
	}

	/**
	 * @param browserWindowSize the browserWindowSize to set
	 */
	public synchronized void setBrowserWindowSize(Dimension browserWindowSize) {
		this.browserWindowSize = browserWindowSize;
		if (controllerToAdjustSize != null) {
			controllerToAdjustSize.requestResize(browserWindowSize.width, browserWindowSize.height);
		}
	}

}
