package com.fernapp.vmcontroller.executor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;


/**
 * A swing application on which tests can be performed on.
 * @author Markus
 */
public class MovingBoxApplication extends JFrame {

	private int objX = 0;
	private int objY = 50;

	public MovingBoxApplication() {
		setTitle("NAaaS Testing Application");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(300, 200));
		setContentPane(new ImagePanel());

		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				moveObject();
			}
		});

		pack();
		System.out.println("Showing test application");
		setVisible(true);
	}

	private void moveObject() {
		objX += 10;

		if (objX > getWidth()) {
			objX = 0;
		}
		repaint();
	}

	private class ImagePanel extends JPanel {
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			g.setColor(Color.YELLOW);
			g.fillRect(0, 0, getWidth(), getHeight());

			g.setColor(Color.BLACK);
			g.fillRect(objX, objY, 50, 50);
		}
	}

	public static void main(String[] args) {
		new MovingBoxApplication();
	}

}
