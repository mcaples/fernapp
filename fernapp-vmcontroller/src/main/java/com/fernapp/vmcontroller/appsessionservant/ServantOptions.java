package com.fernapp.vmcontroller.appsessionservant;

import java.util.ArrayList;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;


/**
 * @author Markus
 * 
 */
public class ServantOptions {
	
	public static final String DISPLAY_DEFAULT = ":5";

	@Option(name = "-httpPort", usage = "the port used by the internal HTTP web server")
	private int httpPort = 8080;
	
	@Option(name = "-port", usage = "RAUP port")
	private int port = 4711;

	@Option(name = "-classicX", usage = "start a classic X server instead of Xvfb")
	private boolean classicX = false;

	@Option(name = "-display", usage = "the X server display port")
	private String display = DISPLAY_DEFAULT;

	@Option(name = "-passphrase", required = true, usage = "passphrase for client authentication")
	private String passphrase;

	@Option(name = "-resources", required = false, usage = "path to fernapp server resources directory")
	private String externalResourcesPath = ".";
	
	@Argument(required = true, metaVar="command", usage= "the application that should be started (including its arguments)")
    private List<String> command = new ArrayList<String>();

	
	public int getHttpPort() {
		return httpPort;
	}

	public void setHttpPort(int httpPort) {
		this.httpPort = httpPort;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isClassicX() {
		return classicX;
	}

	public void setClassicX(boolean classicX) {
		this.classicX = classicX;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	/**
	 * @return the passphrase
	 */
	public String getPassphrase() {
		return passphrase;
	}

	/**
	 * @param passphrase the passphrase to set
	 */
	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}

	/**
	 * @return the externalResourcesPath
	 */
	public String getExternalResourcesPath() {
		return externalResourcesPath;
	}

	/**
	 * @param externalResourcesPath the externalResourcesPath to set
	 */
	public void setExternalResourcesPath(String externalResourcesPath) {
		this.externalResourcesPath = externalResourcesPath;
	}

	public List<String> getCommand() {
		return command;
	}

	public void setCommand(List<String> command) {
		this.command = command;
	}
	
}
