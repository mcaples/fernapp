package com.fernapp.vmcontroller.executor;

import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.vmcontroller.encoding.DamageReport;



/**
 * Interface to listen for window events.
 * @author Markus
 */
public interface WindowEventListener {

	void onWindowSettingsChange(WindowSettings windowSettings, boolean isNewWindow);

	void onWindowDestroyed(String windowId);

	/**
	 * Informs about a content update.
	 */
	void onWindowContentUpdate(String windowId, DamageReport damageReport);

}
