package com.fernapp.vmcontroller.raupserver;

import java.io.IOException;

import com.fernapp.raup.handshake.ClientGreeting;
import com.fernapp.raup.windowmanagement.server.WindowContentUpdate;
import com.fernapp.raup.windowmanagement.server.WindowDestroyed;
import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.uacommon.measurement.AggregatingMeasurementProcessor;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.fernapp.vmcontroller.encoding.DamageReport;
import com.fernapp.vmcontroller.quality.QualityFeedbackService;


/**
 * Represents the server-side of a RAUP connection. Upon creation it registers required
 * encoders at the executor ({@link #init()} must be called). Whenever the connection has
 * send capacity, it sends {@link WindowContentUpdate}s. Might change the encoding over
 * time.
 * <p>
 * When the connection is closed it unregisters all encoders.
 * @author Markus
 */
public interface ServerRaupConnection {

	/**
	 * Processses the {@link ClientGreeting}. Returns true if a RAUP session should be
	 * started with this client. If not, the connection will be closed implicitly.
	 */
	boolean processHandshake(ClientGreeting clientGreeting);

	/**
	 * Initializes the connection and sends the current windows to the client. This method
	 * must have a consistent view of the window data! After the method is finished, this
	 * connection is ready to accept calls to
	 * {@link #onWindowSettingsChange(WindowSettings, boolean)} and
	 * {@link #onWindowDestroyed(WindowDestroyed)}.
	 */
	void initialize();

	/**
	 * Does more expensive initialization (that does not need a consistent view of the
	 * window data).
	 */
	void startUsage() throws IOException;

	/**
	 * Returns if the connection is open.
	 */
	boolean isOpen();

	/**
	 * Returns the state this connection is in.
	 */
	State getState();

	/**
	 * Closes the connection. Does nothing if already closed.
	 */
	void close();

	void onWindowSettingsChange(WindowSettings windowSettings, boolean isNewWindow);

	void onWindowDestroyed(WindowDestroyed windowDestroyed);

	void onWindowContentUpdate(String windowId, DamageReport damageReport);

	QualityFeedbackService getQualityFeedbackService();

	MeasurementReceiver getMeasurementReceiver();

	AggregatingMeasurementProcessor getMeasurementProcessor();

	public static enum State {
		HANDSHAKE_REQUIRED,
		HANDSHAKE_ACCEPTED,
		INITIALIZED,
		ACTIVE,
		CLOSED
	}
}
