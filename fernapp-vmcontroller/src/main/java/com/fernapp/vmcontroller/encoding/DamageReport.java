package com.fernapp.vmcontroller.encoding;

/**
 * Immutable object that describes the damaged area of a window.
 * @author Markus
 */
public class DamageReport {

	private final int x;
	private final int y;
	private final int width;
	private final int height;

	public DamageReport(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public DamageReport mergeWith(DamageReport d2) {
		int x1 = Math.min(this.x, d2.x);
		int y1 = Math.min(this.y, d2.y);
		int x2 = Math.max(this.x + this.width, d2.x + d2.width);
		int y2 = Math.max(this.y + this.height, d2.y + d2.height);
		return new DamageReport(x1, y1, x2 - x1, y2 - y1);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DamageReport [x=" + x + ", y=" + y + ", width=" + width + ", height=" + height + "]";
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

}
