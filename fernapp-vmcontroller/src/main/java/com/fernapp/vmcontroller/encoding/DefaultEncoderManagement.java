package com.fernapp.vmcontroller.encoding;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.VideoEncodingType;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.fernapp.vmcontroller.executor.ApplicationExecutor;
import com.google.common.base.Preconditions;


/**
 * @author Markus
 * 
 */
public class DefaultEncoderManagement implements EncoderManagement {

	private static final Logger log = LoggerFactory.getLogger(DefaultEncoderManagement.class);

	private final ApplicationExecutor applicationExecutor;
	private final MeasurementReceiver measurementReceiver;
	private final Map<String, Encoder> encoders;
	private final ArrayBlockingQueue<Encoder> encodersWithUpdates;

	public DefaultEncoderManagement(ApplicationExecutor applicationExecutor, MeasurementReceiver measurementReceiver) {
		this.applicationExecutor = applicationExecutor;
		this.measurementReceiver = measurementReceiver;
		encoders = new ConcurrentHashMap<String, Encoder>();
		encodersWithUpdates = new ArrayBlockingQueue<Encoder>(20, false);
	}

	/**
	 * @see com.fernapp.uacommon.encoding.EncoderManagement#setEncoder(java.lang.String,
	 * com.fernapp.raup.VideoEncodingType, int)
	 */
	public void setEncoder(final String windowId, VideoEncodingType encodingType, int quality) {
		Preconditions.checkNotNull(windowId);
		Preconditions.checkNotNull(encodingType);
		if (log.isDebugEnabled()) {
			log.debug("Registering encoder for window " + windowId + " (" + encodingType.toString() + ")");
		}

		WindowContentProvider windowContentProvider = new WindowContentProvider() {
			public WindowContent getWindowContent() {
				return applicationExecutor.captureWindowContent(windowId);
			}
		};

		Encoder encoder;
		switch (encodingType) {
		case RAW:
			encoder = new RawEncoder(windowId, windowContentProvider, measurementReceiver);
			break;
		case H264:
			encoder = new H264Encoder(windowId, windowContentProvider, measurementReceiver);
			break;
		case MPNG:
			encoder = new MpngEncoder(windowId, windowContentProvider, measurementReceiver);
			break;
		default:
			throw new IllegalArgumentException("Unsupported encoder");
		}

		Encoder oldEncoder = encoders.put(windowId, encoder);
		if (oldEncoder != null) {
			oldEncoder.shutdown();
		}
		// every newly created encoder has an update
		encodersWithUpdates.offer(encoder);
	}

	/**
	 * @see com.fernapp.uacommon.encoding.EncoderManagement#removeEncoder(java.lang.String)
	 */
	public void removeEncoder(String windowId) {
		Preconditions.checkNotNull(windowId);
		Encoder oldEncoder = encoders.remove(windowId);
		if (oldEncoder == null) {
			throw new IllegalArgumentException("No encoder was registered for window " + windowId);
		}

		oldEncoder.shutdown();
		if (log.isDebugEnabled()) {
			log.debug("Encoder for window " + oldEncoder.getWindowId() + " was removed");
		}
	}

	/**
	 * @see com.fernapp.uacommon.encoding.EncoderManagement#removeAllEncoders()
	 */
	public void removeAllEncoders() {
		for (String windowId : encoders.keySet()) {
			removeEncoder(windowId);
		}
	}

	/**
	 * @see com.fernapp.uacommon.encoding.EncoderManagement#onWindowUpdated(String,
	 * DamageReport)
	 */
	public void onWindowUpdated(String windowId, DamageReport damageReport) {
		Preconditions.checkNotNull(windowId);
		Encoder encoder = encoders.get(windowId);
		if (encoder != null) {
			encoder.onWindowUpdated(damageReport);

			// add to updated queue, if not already present
			// there is a race condition between contains and offer, but no problem
			// because worst case is, one encoder is included twice in the queue
			synchronized (encodersWithUpdates) {
				if (!encodersWithUpdates.contains(encoder)) {
					// if the queue is full we don't add it - no problem
					boolean added = encodersWithUpdates.offer(encoder);
					if (!added) {
						log.warn("The encoder update queue is full");
					}
				}
			}
		}
	}

	/**
	 * @see com.fernapp.uacommon.encoding.EncoderManagement#getEncoder(java.lang.String)
	 */
	public Encoder getEncoder(String windowId) {
		return encoders.get(windowId);
	}

	/**
	 * @see com.fernapp.uacommon.encoding.EncoderManagement#getNextEncoderWithUpdate()
	 */
	public Encoder getNextEncoderWithUpdate() {
		try {
			return encodersWithUpdates.poll(1, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

}
