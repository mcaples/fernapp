package com.fernapp.vmcontroller.encoding;

import com.fernapp.raup.VideoEncodingType;


/**
 * Manages the encoders associates with a RAUP connection.
 * @author Markus
 */
public interface EncoderManagement {

	/**
	 * Allows to set an encoder for the given window. If called again for the same window,
	 * the old encoder will be replaced.
	 */
	void setEncoder(String windowId, VideoEncodingType encodingType, int quality);

	/**
	 * Removes the encoder. Throws an exception if not found.
	 */
	void removeEncoder(String windowId);

	void removeAllEncoders();

	/**
	 * Informs the corresponding encoder of an update. Does nothing if encoder does not
	 * exist.
	 */
	void onWindowUpdated(String windowId, DamageReport damageReport);

	Encoder getEncoder(String windowId);

	/**
	 * Blocks and returns the next encoder that has an update. Returns null if there is no
	 * update after a certain timeout. The encoders with updates are returned in FIFO
	 * order.
	 */
	Encoder getNextEncoderWithUpdate();

}
