package com.fernapp.vmcontroller.encoding;

/**
 * Provides the current content of a specific window.
 * @author Markus
 */
public interface WindowContentProvider {

	/**
	 * Returns the window content or NULL if the window does not exist or some other error
	 * occured.
	 */
	WindowContent getWindowContent();

}
