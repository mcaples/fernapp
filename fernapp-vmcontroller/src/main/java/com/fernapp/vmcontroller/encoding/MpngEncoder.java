package com.fernapp.vmcontroller.encoding;

import com.fernapp.raup.windowmanagement.server.VideoStreamChunk;
import com.fernapp.uacommon.measurement.MeasurementReceiver;


/**
 * Motion PNG encoder.
 * @author Markus
 */
public class MpngEncoder extends AbstractNativeEncoder {

	public MpngEncoder(String windowId, WindowContentProvider windowContentProvider, MeasurementReceiver measurementReceiver) {
		super(windowId, windowContentProvider, measurementReceiver);
	}

	protected native long _init();

	protected native VideoStreamChunk _encode(long windowContentPointer, DamageReport damageReport, long encContext);

	protected native void _changeQuality(int quality, long encContext);

	protected native void _shutdown(long encContext);

}
