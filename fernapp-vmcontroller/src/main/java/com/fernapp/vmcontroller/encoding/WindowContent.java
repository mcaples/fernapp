package com.fernapp.vmcontroller.encoding;

/**
 * Represents WindowContent data in the C address space
 * @author Markus
 */
public class WindowContent {

	private long nativePointer;

	public WindowContent(long nativePointer) {
		this.nativePointer = nativePointer;
	}

	public long getNativePointer() {
		return nativePointer;
	}

	// could implement finalize as a safety net to ensure the native struct has been freed

}
