#include <pthread.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "util/common.h"
#include "x11connector/x11connector.h"
#include "x11connector/x11eventHandler.h"


// manual test program


void *shutdownTask(void *arg) {
	usleep(2 * 1000 * 1000);
	x11Shutdown();
	return NULL;
}


void runX11connector() {
	x11ConnectorInit(":5");

	pthread_t task;
	int arg = 0;
	pthread_create(&task, NULL, shutdownTask, &arg);

	x11EventLoop();
	pthread_join(task,NULL);
}

pid_t run(char **argv) {
	pid_t pid = fork();
	if (pid==0) {
		// child process
		execv(argv[0], argv);
		exit(127); // only if execv fails
	} else {
		// parent process
		return pid;
	}
}

int main() {
	_log = printf_log;

	logInfo("Starting Xvfb");
	char *argv[] = {"/usr/bin/Xvfb", ":5", "-screen", "0", "1600x900x24",NULL};
	pid_t xvfbPid = run(argv);


	// start test app
	// DISPLAY=:1 /usr/bin/gedit
	// "java", "-cp", "target/test-classes", "com.fernapp.vmcontroller.executor.MovingBoxApplication"

	usleep(3 * 1000 * 1000);
	runX11connector();

	WindowId windowId;

	// receive initial windowsettings message
	//			{
	//				RecordedCall call = windowEventListener.waitForCall(5000);
	//				Assert.assertEquals("onWindowSettingsChange", call.getMethodName());
	//				WindowSettings reportedWindowSettings = (WindowSettings) call.getArguments().get(0);
	//				Assert.assertNotNull(reportedWindowSettings.getWindowId());
	//				Assert.assertNull(reportedWindowSettings.getParentWindowId());
	//				Assert.assertEquals(true, call.getArguments().get(1));
	//				windowId = reportedWindowSettings.getWindowId();
	//			}


	// receive initial windowcontentupdate
	//			{
	//				RecordedCall call = windowEventListener.waitForCall(5000);
	//				Assert.assertEquals("onWindowContentUpdate", call.getMethodName());
	//				Assert.assertEquals(windowId, call.getArguments().get(0));
	//			}
	//
	//			TimeUnit.MILLISECONDS.sleep(100);
	//			Assert.assertFalse(windowEventListener.hasRecordedCalls());
	//
	//			for (int i = 0; i < repetitions; i++) {
	//				log.info("Performing click");
	//				PointerButtonEvent input = new PointerButtonEvent(windowId, true, PointerButtonEvent.LEFT_BUTTON);
	//				input.setPointerPositionLeft(50);
	//				input.setPointerPositionTop(50);
	//				appExecutor.sendInputEvent(input);
	//				TimeUnit.MILLISECONDS.sleep(100);
	//				input.setButtonPressed(false);
	//				appExecutor.sendInputEvent(input);
	//
	//				// receive windowcontentupdate
	//				RecordedCall call = windowEventListener.waitForCall(1000);
	//				Assert.assertEquals("onWindowContentUpdate", call.getMethodName());
	//				Assert.assertEquals(windowId, call.getArguments().get(0));
	//
	//				// capture content
	//				appExecutor.captureWindowContent(windowId);
	//
	//				Assert.assertFalse(windowEventListener.hasRecordedCalls());
	//			}
	//
	//			// request close
	//			appExecutor.requestWindowClose(windowId);
	//
	//			// receive destroy
	//			{
	//				RecordedCall call = windowEventListener.waitForCall(1000);
	//				Assert.assertEquals("onDestroyed", call.getMethodName());
	//				Assert.assertEquals(windowId, call.getArguments().get(0));
	//			}
	//
	//		} catch (Exception e) {
	//			log.error("Test failed", e);
	//			throw e;
	//		} finally {
	//			// shutdown
	//			appExecutor.shutdown();
	//		}


	logInfo("Stopping Xvfb");
	kill(xvfbPid, SIGTERM);
	waitpid(xvfbPid, 0, 0);

	return 0;
}
