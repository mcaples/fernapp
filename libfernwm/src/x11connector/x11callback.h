#ifndef X11CALLBACK_H_
#define X11CALLBACK_H_

/**
 * Interface between libfernwm and the outside world (the java app).
 */

#include "../util/common.h"


void onWindowSettingsChange(WindowData *windowData, Bool isNewWindow);

void onDestroy(WindowId windowId);

void onWindowContentUpdate(WindowId windowId, int x, int y, int width, int height);

void reportMeasurement(long category, long long delay, long long dataSize);


#endif
