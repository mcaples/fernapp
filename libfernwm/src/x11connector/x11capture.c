#include <limits.h>
#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xrender.h>
#include <X11/extensions/Xdamage.h>
#include "../util/windowDataManager.h"
#include "x11util.h"
#include "x11callback.h"



XImage *captureImage(Display *dpy, WindowId windowId) {
	XImage *image = NULL;

	lockWindowData();

	WindowData *windowData = getWindowData(windowId,0,0);
	if(windowData != NULL) {
		// windowData->pixmap always points to a valid pixmap
		// only if the window was immediately unmapped after being mapped it might have been impossible for setupCapturing to obtain a pixmap
		if(windowData->pixmap != 0) {
			// get a copy (XImage) of the window content
			X_CATCH_N_LOG( image = XGetImage(dpy, windowData->pixmap, 0, 0, windowData->width, windowData->height, AllPlanes, ZPixmap) );
			if(x11ErrorMessage) {
				image = NULL;
			} else if(image == NULL) {
				logError("XGetImage failed to retrieve image of window %lu", windowId);
			}
		} else {
			logWarn("Unable to capture window content because no pixmap is allocated");
		}
	} else {
		// window has probably already been unmapped
	}

	unlockWindowData();

	return image;
}


WindowContent *x11CaptureWindowContent(WindowId windowId) {
	logTrace("Capturing window content of %lu", windowId);
	unsigned long start = getCurrentTimeMillis();
	WindowContent *wc = NULL;

	Display *dpy = getDisplay();
	XImage *image = captureImage(dpy, windowId);
	if( image != NULL ) {
		// check XImage properties. each pixel is a 4-byte blue,green,red,alpha value (that can be decoded with BufferedImage.TYPE_INT_RGB)
		int expectedDepth = 24;
		Bool validXimage = image->byte_order == LSBFirst && image->bitmap_unit == 32 && image->bitmap_bit_order == LSBFirst && image->bitmap_pad == 32
				&& image->depth == expectedDepth && image->bits_per_pixel == 32;
		if( validXimage ) {
			// TODO crop out the Fluxbox frame

			// construct WindowContent struct
			wc = malloc(sizeof(WindowContent));
			wc->referencesHeld = 1;

			// we are on the safe side if we copy it again. otherwise causes segfaults
			wc->width = image->width;
			wc->height = image->height;
			wc->dataSize = image->bytes_per_line * image->height;
			wc->imageDate = malloc(wc->dataSize);
			wc->pixelFormat = BGRA_PIXEL_FORMAT;
			// we expect unsigned-chars
			ASSERT(CHAR_MIN==0);
			memcpy_wrapper(wc->imageDate, image->data, wc->dataSize);
		} else if( image->depth != expectedDepth) {
			logError("Expected image depth %i but got %i", expectedDepth, image->depth);
		} else {
			logError("Unexpected XImage properties: byte_order=%i, bitmap_unit=%i, bitmap_bit_order=%i, bitmap_pad=%i, depth=%i, bytes_per_line=%i, bits_per_pixel=%i",
					image->byte_order, image->bitmap_unit, image->bitmap_bit_order, image->bitmap_pad, image->depth, image->bytes_per_line, image->bits_per_pixel);
		}

		X_CATCH_N_LOG( XDestroyImage(image) );
	}

	returnDisplay(dpy);

	// measure
	unsigned long delay = getCurrentTimeMillis() - start;
	if( wc != NULL ) {
		reportMeasurement(1, delay, wc->dataSize);
	}

	return wc;
}


/**
 * Setup capturing.
 */
void setupCapturing(Display *dpy, WindowId windowId) {
	lockWindowData();

	WindowData *windowData = getWindowData(windowId,0,0);
	if(windowData != NULL) {
		logTrace("Setting up pixmap for window %lu", windowId);

		// obtain current pixmap. will still be available after the window has been destroyed, or after the window has been resized.
		// if this fails the old pixmap is still available so nothing will break
		X_CATCH_N_LOG( Pixmap newPixmap = XCompositeNameWindowPixmap(dpy, windowId) );
		if(!x11ErrorMessage) {
			// retrieve the dimension of the pixmap
			Window rootWindow;
			int x_return, y_return;
			unsigned int width_return, height_return, border_width_return, depth_return;
			Status ggSuccess = 0;
			X_CATCH_N_LOG( ggSuccess = XGetGeometry(dpy, newPixmap, &rootWindow, &x_return, & y_return, &width_return, &height_return, &border_width_return, &depth_return) );

			if(ggSuccess && !x11ErrorMessage) {
				// register damage detection
				// http://www.x.org/releases/current/doc/damageproto/damageproto.txt
				X_CATCH_N_LOG( XDamageCreate( dpy, newPixmap, XDamageReportBoundingBox ) );

				// free a previously allocated pixmap
				if( windowData->pixmap != 0 ) {
					X_CATCH_N_LOG( XFreePixmap(dpy, windowData->pixmap) );
				}

				windowData->pixmap = newPixmap;
				windowData->width = width_return;
				windowData->height = height_return;
			} else {
				logError("Should not have failed. Universe is broken");
			}
		} else {
			logDebug("XCompositeNameWindowPixmap failed, window %lu might not be visible or has been unmapped in-between", windowId);
		}
	} else {
		fatal("Missing window %lu", windowId);
	}

	unlockWindowData();
}
