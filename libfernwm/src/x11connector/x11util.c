#include "../util/common.h"
#include "x11util.h"


// FIXME concurrency - visibility
GQueue *displayList = NULL;
GStaticRecMutex displayListMutex;

__thread Bool ignoreX11Errors;
__thread char *x11ErrorMessage;
__thread unsigned char x11ErrorCode;

/** the time the server was started in local time */
unsigned long serverStartTime = 0;


void initX11Util() {
	IGNORE_DEPRECATED( g_static_rec_mutex_init(&displayListMutex) );

	displayList = g_queue_new();
}


void shutdownX11Util() {
	closeAllDisplays();
	IGNORE_DEPRECATED( g_static_rec_mutex_free(&displayListMutex) );
	g_queue_free(displayList);
}


Display *getDisplay() {
	IGNORE_DEPRECATED( g_static_rec_mutex_lock(&displayListMutex) );

	Display *dpy = (Display*) g_queue_pop_head(displayList);

	if(dpy == NULL) {
		logInfo("Opening new connection to display %s", displayName);
		dpy = XOpenDisplay(displayName);
		if (!dpy) {
			logError("Unable to connect to display %s", displayName);
		}

		XSetErrorHandler(x11ErrorHandler);
		ignoreX11Errors = False;
		x11ErrorMessage = NULL;
		x11ErrorCode = 0;
	}

	IGNORE_DEPRECATED( g_static_rec_mutex_unlock(&displayListMutex) );
	return dpy;
}


void returnDisplay(Display *dpy) {
	logTrace("Returning display to pool");
	IGNORE_DEPRECATED( g_static_rec_mutex_lock(&displayListMutex) );
	g_queue_push_head(displayList, dpy);
	IGNORE_DEPRECATED( g_static_rec_mutex_unlock(&displayListMutex) );
}


void closeAllDisplays() {
	IGNORE_DEPRECATED( g_static_rec_mutex_lock(&displayListMutex) );
	Display *dpy;
	while( (dpy = (Display*) g_queue_pop_head(displayList)) != NULL ) {
		logTrace("Closing display from pool");
		XCloseDisplay(dpy);  // performs XSync internally
	}
	IGNORE_DEPRECATED( g_static_rec_mutex_unlock(&displayListMutex) );
}


unsigned long getPropertyValueAsLu(Display *dpy, Window win, Atom propertyAtom, Bool logErrorOnFailure) {
    Atom actual_type;
    int actual_format;
    unsigned long nitems, bytes_after;
    unsigned char *prop_return = NULL;

    int ret = XGetWindowProperty(
    		dpy, win, propertyAtom, 0L, sizeof(Atom), False, AnyPropertyType, &actual_type,
            &actual_format, &nitems, &bytes_after,
            &prop_return);

    //logDebug("XGetWindowProperty: actual_type=%lu, actual_format=%i, nitems=%lu, bytes_after=%lu\n", actual_type, actual_format, nitems, bytes_after);

    if(Success == ret && prop_return) {
    	ASSERT( actual_type == XA_ATOM || actual_type == XA_WINDOW );
    	ASSERT( actual_format == 32 ); // property length in bits
    	ASSERT( nitems == 1 );
    	ASSERT( bytes_after == 0 );

    	unsigned long value = *(unsigned long *)prop_return;
        XFree(prop_return);
        return value;
    } else {
    	if(logErrorOnFailure) {
			char *atomName = XGetAtomName(dpy, propertyAtom);
			if(actual_format == None) {
				logError("XGetWindowProperty failed. Property %s does not exist on window %lu", atomName, win);
			} else {
				logError("XGetWindowProperty failed for property %s on window %lu", atomName, win);
			}
			XFree(atomName);
    	}
    	return 0;
    }
}


Bool isInnerWindow(Display *dpy, WindowId windowId) {
	Bool innerWindow = False;
	XClassHint hint;
    X_CATCH_N_LOG( Status xSuccess = XGetClassHint(dpy, windowId, &hint) );
    if(xSuccess) {
    	innerWindow = True;
    	XFree(hint.res_name);
    	XFree(hint.res_class);
    }
    return innerWindow;
}


Window identifyInnerWindow(Display *dpy, Window windowId) {
	// logDebug("identifyInnerWindow of %lu", windowId);

	if( isInnerWindow(dpy,windowId) ) {
		// no re-parenting
		return windowId;
	} else {
		Window root_win, parent_win;
		unsigned int num_children;
		Window *child_list;
		X_CATCH_N_LOG( Status xSuccess = XQueryTree(dpy, windowId, &root_win, &parent_win, &child_list, &num_children) );
		if(xSuccess) {
			Window innerWindowId = windowId;
			for(int i=0; i<num_children; i++) {
				Window childId = child_list[i];
				if( isInnerWindow(dpy,childId) ) {
					innerWindowId = childId;
					break;
				}
			}

			if( child_list != NULL ) {
				XFree(child_list);
			}
			return innerWindowId;
		} else {
			logWarn("XQueryTree failed for window %lu", windowId);
			return 0;
		}
	}
}


int x11ErrorHandler(Display *dpy, XErrorEvent *errorEvent) {
	clearX11ErrorMessage();
	x11ErrorCode = errorEvent->error_code;
	ASSERT(x11ErrorCode != 0);

	const int bufferSize = 500;
	char errorMessageBuffer[bufferSize];
	XGetErrorText(dpy, errorEvent->error_code, errorMessageBuffer, bufferSize);
	int msgLength = strlen(errorMessageBuffer) + 1;
	x11ErrorMessage = malloc(msgLength);
	strcpy(x11ErrorMessage, errorMessageBuffer);

	if(!ignoreX11Errors) {
		logError("Fatal error received from X server: %s", x11ErrorMessage);
		fatal("Fatal X11 error received");
	}
	return 0;
}


void clearX11ErrorMessage() {
	x11ErrorCode = 0;
	if(x11ErrorMessage != NULL) {
		free(x11ErrorMessage);
		x11ErrorMessage = NULL;
	}
}


Time getServerTime() {
	if( serverStartTime == 0 ) {
		logWarn("Server start time not yet determined");
		return 0;
	}
	unsigned long serverUptime = getCurrentTimeMillis() - serverStartTime;
	return serverUptime;
}


void informAboutServerTime(Time serverUptime) {
	//logDebug("serverUptime %lu, st=%lu", serverUptime, serverStartTime);
	unsigned long newServerStartTime = getCurrentTimeMillis() - serverUptime;
	if( serverStartTime != 0 ) {
		unsigned long delta = abs(newServerStartTime-serverStartTime);
		if( delta > 1000 ) {
			logWarn("Server time varies greatly. Last determined %lu, difference %lu", serverStartTime, delta);
		}

		int stabilityFactor = 5;
		serverStartTime = (((double)serverStartTime*(stabilityFactor-1)) + newServerStartTime) / stabilityFactor;
	} else {
		serverStartTime = newServerStartTime;
	}
}
