#ifndef X11UTIL_H_
#define X11UTIL_H_


/**
 * General purpose X11 utility stuff. Not specific to fernapp.
 */

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>


/**
 * Catches X errors that occur during execution (and thus preventing our default X11 error handler to raise a SIGSEGV).
 * This should always be used for X functions that don't return a value, and thus work asynchronously and don't report errors directly.
 * In case of an error x11ErrorMessage will point to the error message, otherwise it is NULL.
 * Might also report an error that relates to an earlier X operation without X_CATCH
 */
#define X_CATCH(expr) \
	clearX11ErrorMessage(); \
	ignoreX11Errors = True; \
	expr; \
	/* flush output buffer and process responses (otherwise errors will be reported during a later call)*/ \
	XSync(dpy, False); \
	ignoreX11Errors = False;


/** Like X_CATCH but logs a warning in case of an error */
#define X_CATCH_N_LOG(expr) \
	X_CATCH(expr); \
	if( x11ErrorMessage ) { \
		logWarn("%s. Caused by: %s", x11ErrorMessage, #expr); \
	}


/** Like X_CATCH but logs a warning in case of an error and issues a return from the current function */
#define X_FAIL_FAST(expr) \
	X_CATCH(expr); \
	if( x11ErrorMessage ) { \
		logWarn("%s. Caused by: %s", x11ErrorMessage, #expr); \
		return; \
	}


char *displayName;

void initX11Util();
void shutdownX11Util();


/**
 * Returns a pooled Display connection or creates a new one.
 * Xlib is not thread-safe. Every thread must use its own Display connection.
 */
Display *getDisplay();

/**
 * Returns the display connection to the pool.
 */
void returnDisplay(Display *dpy);


/**
 * Closes all pooled display connections.
 */
void closeAllDisplays();


/**
 * Retrieves a property as an unsigned long. Can be used for Atom and Window values.
 * Returns 0 on error.
 */
unsigned long getPropertyValueAsLu(Display *dpy, Window windowId, Atom propertyAtom, Bool logErrorOnFailure);


/**
 * Identifies the inner window and ignores the window manager stuff (if inner window was re-parented).
 * Returns the windowId if it was not re-parented. Returns 0 if there was an error.
 */
Window identifyInnerWindow(Display *dpy, Window windowId);


// every thread has its own error handling
extern __thread Bool ignoreX11Errors;
extern __thread char *x11ErrorMessage;
extern __thread unsigned char x11ErrorCode;
int x11ErrorHandler(Display *dpy, XErrorEvent *errorEvent);
void clearX11ErrorMessage();


Time getServerTime();
void informAboutServerTime(Time serverTime);


#endif /* X11UTIL_H_ */
