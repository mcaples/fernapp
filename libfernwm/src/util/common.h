#ifndef UTIL_H_
#define UTIL_H_

/**
 * Utilities for this C library.
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <signal.h>
#include <glib.h>
#include <stdint.h>
#include <inttypes.h>
#include "logging.h"


typedef unsigned long WindowId;
#define Bool int
#define True 1
#define False 0


typedef struct WindowData_type {
	WindowId windowId;
	WindowId innerWindowId;
	unsigned long pixmap;
	int width;
	int height;
	char *title;
	WindowId parentWindowId;
	int positionX;
	int positionY;
} WindowData;


#define BGRA_PIXEL_FORMAT 0
#define RGB_PIXEL_FORMAT 1

typedef struct WindowContent_type {
	int width;
	int height;
	int dataSize;
	/** pixel format is 4-byte BGRA */
	uint8_t *imageDate;
	int pixelFormat;

	/** Increment it whenever it is handed out to another function */
	int referencesHeld;
} WindowContent;


typedef struct DamageReport_type {
	int damageX;
	int damageY;
	int damageWidth;
	int damageHeight;
} DamageReport;


#define ASSERT(expr) \
	{ \
		Bool assertValid = expr; \
		if( !assertValid ) { \
			logError("Assertion '%s' failed", #expr); \
			raise(SIGSEGV); \
		} \
	}

void freeWindowContent(WindowContent *windowContent);


unsigned long getCurrentTimeMillis();


#ifndef __GLIBC_COMPAT_SYMBOL_H__
#define __GLIBC_COMPAT_SYMBOL_H__ 1
	#ifdef __amd64__
	   #define GLIBC_COMPAT_SYMBOL(FFF) __asm__(".symver " #FFF "," #FFF "@GLIBC_2.2.5");
	#else
	   #define GLIBC_COMPAT_SYMBOL(FFF) __asm__(".symver " #FFF "," #FFF "@GLIBC_2.0");
	#endif /*__amd64__*/
#endif /*__GLIBC_COMPAT_SYMBOL_H__*/


void *memcpy_wrapper(void *dest, void *src, int n);


/**
 * Terminates the program due to a fatal error.
 */
void fatal(char* reason, ...);


#define IGNORE_DEPRECATED(expr) \
		_Pragma("GCC diagnostic push") \
		_Pragma("GCC diagnostic ignored \"-Wdeprecated-declarations\"") \
		expr; \
		_Pragma("GCC diagnostic pop")


#endif /* UTIL_H_ */
