#ifndef LOGGING_H_
#define LOGGING_H_


#define LOG_TRACE 0
#define LOG_DEBUG 1
#define LOG_INFO 2
#define LOG_WARN 3
#define LOG_ERROR 4


void logTrace(char* message, ...);
void logDebug(char* message, ...);
void logInfo(char* message, ...);
void logWarn(char* message, ...);
void logError(char* message, ...);


/**
 * Internal logging. Used by the other functions.
 */
void (*_log)(int logLevel, char* message);

void _logf(int logLevel, char* message, va_list args);

void printf_log(int logLevel, char* message);

#endif /* LOGGING_H_ */
