package com.fernapp.uacommon.measurement;

import java.awt.Font;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.TabSet;
import javax.swing.text.TabStop;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.measurement.AggregatingMeasurementProcessor.MeasurementReport;


/**
 * @author Markus
 * 
 */
public class MeasurementReportFrame extends JFrame {

	private static final Logger log = LoggerFactory.getLogger(MeasurementReportFrame.class);

	private AggregatingMeasurementProcessor processor;
	private JTextPane reportPane;

	public MeasurementReportFrame(AggregatingMeasurementProcessor processor) throws HeadlessException {
		this.processor = processor;

		setTitle("NAaaS Performance");

		reportPane = new JTextPane();
		add(reportPane);
		reportPane.setFont(new Font("Monospaced", Font.PLAIN, 14));

		TabStop[] tabs = new TabStop[4];
		tabs[0] = new TabStop(2, TabStop.ALIGN_LEFT, TabStop.LEAD_DOTS);
		tabs[1] = new TabStop(200, TabStop.ALIGN_RIGHT, TabStop.LEAD_DOTS);
		tabs[2] = new TabStop(350, TabStop.ALIGN_RIGHT, TabStop.LEAD_DOTS);
		tabs[3] = new TabStop(450, TabStop.ALIGN_RIGHT, TabStop.LEAD_DOTS);
		TabSet tabset = new TabSet(tabs);

		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.TabSet, tabset);
		reportPane.setParagraphAttributes(aset, false);

		updateReport();
		pack();

		new Thread(new Runnable() {
			public void run() {
				try {
					while (isVisible()) {
						Thread.sleep(1000);
						updateReport();
					}
				} catch (InterruptedException e) {
					log.error("reportUpdater failed", e);
				}
			}
		}, "reportUpdater").start();
	}

	private void updateReport() {
		StringBuffer sb = new StringBuffer();
		sb.append("\tCategory\tAvg. Delay\tAvg. Data Size\tCount\n");
		for (DelayCategory delayCategory : DelayCategory.values()) {
			MeasurementReport mr = processor.generateReport(delayCategory);
			if (mr != null) {
				sb.append("\t");
				sb.append(delayCategory.toString());
				sb.append("\t");
				sb.append((int) mr.getAvgDelay());
				sb.append('\t');
				sb.append((int) mr.getAvgDataSize());
				sb.append('\t');
				sb.append(mr.getCount());
				sb.append('\n');
			}
		}

		reportPane.setText(sb.toString());
		validate();
	}

}
