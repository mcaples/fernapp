package com.fernapp.uacommon.measurement;

import com.fernapp.raup.windowmanagement.server.WindowContentUpdate;


/**
 * The different categories of delays that make up the total delay of delivering a window
 * content update.
 * @author Markus
 */
public enum DelayCategory {

	/**
	 * The processing delay caused by the application.
	 */
	APPLICATION,

	/**
	 * Delay until processing of a content update begins. More precisely, the delay
	 * between the oldest update that has not yet been processed and the beginning of the
	 * encoding of that update. Other updates might have occurred in between.
	 */
	WAIT,

	/**
	 * Time to capture and transfer a window snapshot into our address space.
	 */
	CAPTURE,

	/**
	 * Chunk encode time.
	 */
	ENCODE,

	/**
	 * End-to-end delay for a {@link WindowContentUpdate} message. Caused by the messaging
	 * middleware and the network. Made up of the packet delay and buffering delays due to
	 * the limited bandwidth.
	 */
	MIDDLEWARE,

	/**
	 * Chunk decode time.
	 */
	DECODE

}
