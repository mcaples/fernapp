package com.fernapp.uacommon.middleware;

import java.io.Serializable;


/**
 * A dummy message to tell the other party that we are still alive (keep alive).
 * @author Markus
 */
public class KeepAlive implements Serializable {

	// no payload

}
