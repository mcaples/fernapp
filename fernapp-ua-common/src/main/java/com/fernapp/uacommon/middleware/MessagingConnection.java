package com.fernapp.uacommon.middleware;

import java.io.IOException;

import com.fernapp.util.Callback;


/**
 * A simple message-based middleware. Messages will be serialized and deserialzed by a
 * implementation specific method (JSON, XML, Java serialization, ...). Is based on a
 * persistent (network) connection to the other side. Automatically detects if the other
 * side is gone (keep alive). Implementations must be thread-safe!
 * <p>
 * In order to detect a disconnected party, both sides must send a message at least every
 * {@link #CONNECTION_TIMEOUT} milliseconds. For this purpose a party may send
 * {@link KeepAlive}s. If no message was received {@link #close()} will be called
 * internally.
 * <p>
 * This is a dumb message-based middleware which does not care about authentication or
 * data encryption.
 * 
 * @author Markus
 */
public interface MessagingConnection {

	/**
	 * Timeout in milliseconds.
	 */
	public static final long CONNECTION_TIMEOUT = 30 * 1000;

	/**
	 * Returns if the connections is still open.
	 */
	boolean isOpen();

	/**
	 * Registers a close listener. Will be triggered immediately if connection is already
	 * closed.
	 */
	void setOnCloseListener(Callback<Void> onCloseCallback);

	/**
	 * Closes the connection and invokes the on-close-listener. May be called multiple
	 * times.
	 */
	void close();

	/**
	 * Same as {@link #sendMessage(Object)} but sends the message asynchronously in the
	 * background. The call returns immediately. If send fails an error will be logged.
	 */
	void sendMessageAsync(Object message);

	/**
	 * Sends the given message. Will block if send buffer is full. In the case of an
	 * {@link IOException}, {@link #close()} will be called automatically.
	 */
	void sendMessage(Object message) throws IOException;

	/**
	 * Registers the given handler for the specified message type. Might override a
	 * previously registered handler. Received events will be delivered to the most
	 * concrete registered handler.
	 */
	<T> void registerReceivedHandler(Class<T> messageClazz, Callback<T> receiveCallback);

	/**
	 * Usually this should be called after the calls to
	 * {@link #registerReceivedHandler(Class, Callback)} to ensure no messages can be
	 * missed. Exceptions thrown by receive handlers will be logged but the receive loop
	 * will continue. Must only be called once. Also starts the alive check of the other
	 * side.
	 */
	void startReceivingMessages();

	/**
	 * Something that identifies the remote endpoint (e.g. IP address).
	 */
	String getRemoteEndpointDescriptor();

	/**
	 * Returns a name for this connection.
	 */
	String getName();

}
