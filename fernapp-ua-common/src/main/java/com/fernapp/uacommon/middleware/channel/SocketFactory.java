package com.fernapp.uacommon.middleware.channel;

import java.net.Socket;
import java.net.SocketException;


/**
 * @author Markus
 * 
 */
public class SocketFactory {

	public static void configureServerSocket(Socket socket) throws SocketException {
		socket.setTcpNoDelay(true);
		socket.setSendBufferSize(400000);
	}

	public static Socket createClientSocket() throws SocketException {
		Socket socket = new Socket();
		socket.setTcpNoDelay(true);
		socket.setReceiveBufferSize(400000);
		return socket;
	}

}
