package com.fernapp.useragent.common;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;


/**
 * @author Markus
 * 
 */
public enum ActionDelayMeasurer {

	INSTANCE;

	private static final Logger log = LoggerFactory.getLogger(ActionDelayMeasurer.class);

	private Stopwatch stopwatch = new Stopwatch();

	private Long[] recentDelays = new Long[5];
	private int recentDelaysPos = 0;
	private long sum;
	private int count;
	private long avgDelay;

	public synchronized void onActionStart() {
		// don't overwrite an already set start info. we expect some feedback for every
		// event!
		if (!stopwatch.isRunning()) {
			stopwatch.start();
		}
	}

	public synchronized void onActionEnd() {
		if (stopwatch.isRunning()) {
			stopwatch.stop();
			long delay = stopwatch.elapsedMillis();
			stopwatch.reset();

			recentDelays[recentDelaysPos++] = delay;
			if (recentDelaysPos >= recentDelays.length) {
				recentDelaysPos = 0;
			}

			updateStatistics();
			log.info("Measured action delay is " + delay + "ms (avg. of last " + count + " measurements is " + avgDelay + "ms)");
		}
	}

	public synchronized long getAvgDelay() {
		updateStatistics();
		return avgDelay;
	}

	public void updateStatistics() {
		sum = 0;
		count = 0;
		for (Long num : recentDelays) {
			if (num != null) {
				sum += num;
				count++;
			}
		}
		if (count != 0) {
			avgDelay = sum / count;
		}
	}

	public synchronized void reset() {
		Arrays.fill(recentDelays, null);
	}

}
