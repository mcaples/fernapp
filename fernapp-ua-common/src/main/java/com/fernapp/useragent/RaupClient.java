package com.fernapp.useragent;

import java.util.List;

import com.fernapp.raup.handshake.ClientGreeting;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.useragent.ui.RemoteWindowController;


/**
 * A RAUP client. Fullfills the requirements of the RAUP protocol and keeps track of the
 * local windows (can be considered a window manager).
 * @author Markus
 */
public interface RaupClient {

	/**
	 * Informs the RAUP client that it should now serve the given
	 * {@link MessagingConnection}. The client should first send a {@link ClientGreeting}
	 * to the server. Will be called again if connection has been lost or has been
	 * reconnected.
	 * @param messagingConnection the {@link MessagingConnection} or null if the
	 * connection has been lost
	 */
	void initForConnection(MessagingConnection messagingConnection);

	/**
	 * Returns the underlying {@link MessagingConnection} or null if currently not
	 * connected to a server. The returned connection must not be cached because it might
	 * change.
	 */
	MessagingConnection getMessagingConnection();

	/**
	 * Shut down of the RAUP client. Closes the {@link MessagingConnection} and all
	 * locally opened windows.
	 */
	void shutdown();

	/**
	 * Returns a {@link MeasurementReceiver} that delegates measurements to the server.
	 */
	MeasurementReceiver getMeasurementReceiver();

	/**
	 * Returns the parent of a given {@link RemoteWindowController}, or throws an
	 * exception. Does not return null.
	 */
	RemoteWindowController getParentWindow(RemoteWindowController remoteWindowController);

	/**
	 * Returns all open windows currently known by the client.
	 */
	List<RemoteWindowController> getWindows();

}
