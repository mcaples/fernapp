package com.fernapp.useragent.ui;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.VideoEncodingType;
import com.fernapp.raup.windowmanagement.client.WindowCloseRequest;
import com.fernapp.raup.windowmanagement.client.WindowResizeRequest;
import com.fernapp.raup.windowmanagement.server.VideoStreamChunk;
import com.fernapp.raup.windowmanagement.server.WindowContentUpdate;
import com.fernapp.raup.windowmanagement.server.WindowDestroyed;
import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.uacommon.measurement.DelayCategory;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.useragent.RaupClient;
import com.fernapp.useragent.common.ActionDelayMeasurer;
import com.fernapp.useragent.decoder.Decoder;
import com.fernapp.useragent.decoder.H264Decoder;
import com.fernapp.useragent.decoder.MpngDecoder;
import com.fernapp.useragent.decoder.RawDecoder;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;


/**
 * Represents and controls a remote window in the user agent. The actual remote window and
 * its video stream are display by a {@link WindowContainer}.
 * <p>
 * It is expected that its methods will always be called from the same thread. It's not
 * thread-safe!
 * @author Markus
 */
public class RemoteWindowController {

	private static final Logger log = LoggerFactory.getLogger(RemoteWindowController.class);

	private final String windowId;
	private final WindowContainer windowContainer;
	private final RaupClient raupClient;
	/** used to check that there is no multi-threaded access */
	private final Thread thread;

	private volatile WindowState _state = WindowState.INITIALIZING;
	private WindowSettings windowSettings;
	private InputEventSender inputEventSender;
	private Decoder decoder;
	private Long windowDimensionIncorrentStartTimestamp;
	private long maxWindowDimensionIncorrentMillis = 1000;

	private final Executor asyncTasks = Executors.newSingleThreadExecutor();

	// cropping for fluxbox window manager (fluxbox style: bora black)
	// private final int cropNorth = 23;
	// private final int cropSouth = 4;
	// private final int cropWest = 1;
	// private final int cropEast = 1;
	private final int cropNorth = 0;
	private final int cropSouth = 0;
	private final int cropWest = 0;
	private final int cropEast = 0;

	private static enum WindowState {
		INITIALIZING,
		AWAITING_FIRST_FRAME,
		VISIBLE,
		CLOSED
	}

	public RemoteWindowController(String windowId, WindowContainer windowContainer, RaupClient raupClient) {
		log.info("Remote window controller initializing for window " + windowId);
		this.windowId = windowId;
		this.windowContainer = windowContainer;
		this.raupClient = raupClient;
		this.thread = Thread.currentThread();
	}

	private void init() {
		Preconditions.checkArgument(getState() == WindowState.INITIALIZING);
		Preconditions.checkState(thread == Thread.currentThread());

		inputEventSender = new InputEventSender(windowId, raupClient);
		windowContainer.addInputEventListener(inputEventSender);

		windowContainer.addWindowStructuralListener(new WindowStructuralListener() {
			public void resized(int width, int height, boolean causedByUser) {
				if (causedByUser) {
					requestResize(width, height);
				}
			}

			public void closing() {
				// only the server can close a window (the server might decide to show a
				// "are you sure?" window before that)
				log.info("User requests window closing");

				// do async. otherwise it might block the entire GUI!
				asyncTasks.execute(new Runnable() {
					public void run() {
						try {
							// request close and confirmation
							WindowCloseRequest wcr = new WindowCloseRequest(windowId);
							MessagingConnection mc = raupClient.getMessagingConnection();
							mc.sendMessageAsync(wcr);
						} catch (Exception e) {
							log.error("Async closing failed", e);
						}
					}
				});
			}
		});

		setState(WindowState.AWAITING_FIRST_FRAME);
	}

	/**
	 * @see com.fernapp.useragent.ui.RemoteWindowEventListener#onWindowSettingsChange(com.fernapp.raup.windowmanagement.server.WindowSettings)
	 */
	public void onWindowSettingsChange(WindowSettings ws, boolean isNewWindow) {
		Preconditions.checkArgument(ws.getWindowId().equals(windowId));
		Preconditions.checkState(thread == Thread.currentThread());

		if (log.isDebugEnabled()) {
			log.debug("Received window settings " + ws.toString());
		}
		this.windowSettings = ws;

		// position
		if (windowSettings.getParentWindowId() == null) {
			if (isNewWindow) {
				Dimension screenSize;
				if (GraphicsEnvironment.isHeadless()) {
					screenSize = new Dimension(100, 100);
				} else {
					screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				}
				int x = screenSize.width / 2 - (windowSettings.getWidth() / 2);
				x = Math.max(x, 0);
				int y = screenSize.height / 2 - (windowSettings.getHeight() / 2);
				y = Math.max(y, 0);
				windowContainer.setContentLocation(x, y);
			}
		} else {
			// server sends events in causal correct order - thus there will always be a
			// parent
			RemoteWindowController parentWin = raupClient.getParentWindow(this);

			int relX = windowSettings.getPositionX();
			int relY = windowSettings.getPositionY();

			if (parentWin.getWindowSettings().getParentWindowId() == null) {
				// the parent is a normal window (i.e. is decorated by fluxbox)
				// fluxbox frame must be removed
				relX -= cropWest;
				relY -= cropNorth;
			}
			int x = parentWin.getWindowContainer().getContentLocationLeft() + relX;
			int y = parentWin.getWindowContainer().getContentLocationTop() + relY;
			windowContainer.setContentLocation(x, y);
		}

		windowContainer.setTitle(windowSettings.getTitle());

		if (getState() == WindowState.INITIALIZING) {
			init();
		}
	}

	/**
	 * @see com.fernapp.useragent.ui.RemoteWindowEventListener#onWindowDestroyed(com.fernapp.raup.windowmanagement.server.WindowDestroyed)
	 */
	public void onWindowDestroyed(WindowDestroyed windowDestroyed) {
		Preconditions.checkArgument(windowDestroyed.getWindowId().equals(windowId));
		Preconditions.checkState(thread == Thread.currentThread());

		log.info("Received destroy for window " + windowId);
		dispose();
	}

	public void dispose() {
		Preconditions.checkState(getState() == WindowState.INITIALIZING || getState() == WindowState.AWAITING_FIRST_FRAME || getState() == WindowState.VISIBLE);
		// TODO will be called from another thread
		// Preconditions.checkState(thread == Thread.currentThread());

		setState(WindowState.CLOSED);
		windowContainer.dispose();
		if (inputEventSender != null) {
			inputEventSender.dispose();
		}
		if (decoder != null) {
			decoder.shutdown();
		}
	}

	/**
	 * @see com.fernapp.useragent.ui.RemoteWindowEventListener#onWindowContentUpdate(com.fernapp.raup.windowmanagement.server.WindowContentUpdate)
	 */
	public void onWindowContentUpdate(WindowContentUpdate windowContentUpdate) {
		Preconditions.checkArgument(windowContentUpdate.getWindowId().equals(windowId));
		Preconditions.checkState(thread == Thread.currentThread());

		if (log.isTraceEnabled()) {
			log.trace("Received content update for window " + windowId);
		}

		VideoStreamChunk chunk = windowContentUpdate.getVideoStreamChunk();
		assert (chunk.getData() != null);
		assert (chunk.getWidth() > 0);
		assert (chunk.getHeight() > 0);

		// shutdown old decoder if a reset or type has changed
		if (decoder != null && (chunk.isReset() || chunk.getEncoderType() != decoder.getEncodingType().getNativeEncodingType())) {
			decoder.shutdown();
			decoder = null;
		}
		// create new decoder if necessary
		if (decoder == null) {
			if (chunk.getEncoderType() == VideoEncodingType.RAW.getNativeEncodingType()) {
				decoder = new RawDecoder();
			} else if (chunk.getEncoderType() == VideoEncodingType.H264.getNativeEncodingType()) {
				decoder = new H264Decoder();
			} else if (chunk.getEncoderType() == VideoEncodingType.MPNG.getNativeEncodingType()) {
				decoder = new MpngDecoder();
			} else {
				throw new IllegalArgumentException("Unsupported encoder " + chunk.getEncoderType());
			}
		}

		// decode and measure
		Stopwatch stopwatch = new Stopwatch().start();
		BufferedImage bufferedImage = decoder.decode(chunk.getWidth(), chunk.getHeight(), chunk.getData());
		stopwatch.stop();
		raupClient.getMeasurementReceiver().receiveMeasurement(DelayCategory.DECODE, stopwatch.elapsedMillis(), 0);
		assert (bufferedImage.getWidth() == chunk.getWidth());
		assert (bufferedImage.getHeight() == chunk.getHeight());

		if (windowSettings.getParentWindowId() == null) {
			// hack: its a main window. crop away the window frame added by Fluxbox
			BufferedImage croppedImage = bufferedImage.getSubimage(cropWest, cropNorth, bufferedImage.getWidth() - cropWest - cropEast, bufferedImage.getHeight() - cropNorth
					- cropSouth);
			// System.out.println("orgImage width: " + bi.getWidth() + ", height: " +
			// bi.getHeight());
			// System.out.println("croppedImage width: " + croppedImage.getWidth() +
			// ", height: " + croppedImage.getHeight());
			bufferedImage = croppedImage;
		}

		windowContainer.updateContent(bufferedImage);

		if (getState() == WindowState.AWAITING_FIRST_FRAME) {
			// make it visible
			windowContainer.setContentSize(bufferedImage.getWidth(), bufferedImage.getHeight());
			windowContainer.show();
			setState(WindowState.VISIBLE);

		} else if (getState() == WindowState.VISIBLE) {
			assert (windowContainer.isVisible());

			// adjust size - makes sure that the window dimension remains incorrect no
			// longer than maxWindowDimensionIncorrentMillis
			if (windowContainer.getContentWidth() == bufferedImage.getWidth() && windowContainer.getContentHeight() == bufferedImage.getHeight()) {
				windowDimensionIncorrentStartTimestamp = null;
			} else {
				if (windowDimensionIncorrentStartTimestamp != null) {
					long incorrentTime = System.currentTimeMillis() - windowDimensionIncorrentStartTimestamp;
					if (incorrentTime > maxWindowDimensionIncorrentMillis) {
						windowContainer.setContentSize(bufferedImage.getWidth(), bufferedImage.getHeight());
					} else {
						// we'll wait a bit longer, maybe the server will send frames with
						// the correct dimension soon
					}
				} else {
					windowDimensionIncorrentStartTimestamp = System.currentTimeMillis();
				}
			}
		} else {
			log.warn("Not showing window update because in state " + getState().toString());
		}

		ActionDelayMeasurer.INSTANCE.onActionEnd();
	}

	public void requestResize(int width, int height) {
		Preconditions.checkArgument(width > 0);
		Preconditions.checkArgument(height > 0);
		WindowResizeRequest wrr = new WindowResizeRequest(windowId, width, height);
		if (log.isTraceEnabled()) {
			log.trace("Sending window resize request " + wrr);
		}
		MessagingConnection mc = raupClient.getMessagingConnection();
		mc.sendMessageAsync(wrr);
	}

	/**
	 * @return the windowSettings
	 */
	public WindowSettings getWindowSettings() {
		Preconditions.checkState(thread == Thread.currentThread());
		return windowSettings;
	}

	/**
	 * Returns the window ID (is thread-safe).
	 */
	public String getWindowId() {
		return windowId;
	}

	/**
	 * @return the windowContainer
	 */
	public WindowContainer getWindowContainer() {
		Preconditions.checkState(thread == Thread.currentThread());
		return windowContainer;
	}

	/**
	 * @see java.awt.Component#toString()
	 */
	@Override
	public String toString() {
		return "Remote window controller (window " + windowId + ")";
	}

	/**
	 * Returns the state (is thread-safe).
	 */
	public WindowState getState() {
		return _state;
	}

	/**
	 * Changes the state (is thread-safe).
	 */
	public void setState(WindowState newState) {
		if (log.isDebugEnabled()) {
			log.debug("Changing state from " + getState().toString() + " to " + newState.toString() + " (window " + windowId + ")");
		}
		this._state = newState;
	}

}
