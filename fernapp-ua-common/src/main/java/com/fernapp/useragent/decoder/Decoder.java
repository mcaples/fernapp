package com.fernapp.useragent.decoder;

import java.awt.image.BufferedImage;

import com.fernapp.raup.VideoEncodingType;
import com.fernapp.raup.windowmanagement.server.VideoStreamChunk;


/**
 * A decoder. Does not need to be thread-safe.
 * @author Markus
 */
public interface Decoder {

	VideoEncodingType getEncodingType();

	/**
	 * Decodes the encoded data (from a {@link VideoStreamChunk}).
	 */
	BufferedImage decode(int width, int height, byte[] encodedChunk);

	void shutdown();

}
