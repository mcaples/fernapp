package com.fernapp.util;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


/**
 * @author Markus
 */
public class NamedThreadFactory implements ThreadFactory {

	private final String name;

	public NamedThreadFactory(String name) {
		this.name = name;
	}

	/**
	 * @see java.util.concurrent.ThreadFactory#newThread(java.lang.Runnable)
	 */
	public Thread newThread(Runnable r) {
		Thread t = Executors.defaultThreadFactory().newThread(r);
		t.setName(name);
		return t;
	}
}
