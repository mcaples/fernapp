package com.fernapp.raup.windowmanagement.server;

import java.io.Serializable;


/**
 * Informs the client of the settings of a window. If the client has not heard of a
 * certain window before, it means that the window is new.
 * <p>
 * {@link WindowSettings} messages are delivered in causal order. Meaning on window
 * creation, information about parent windows will be delivered before messages for its
 * children.
 * <p>
 * Two types of windows exist: Normal and popup windows. A normal window does have a title
 * bar and appears in the task bar. {@link #parentWindowId} will be null for them. A popup
 * window does not have a title bar and does not appear in the task bar. It is a child of
 * a normal window ({@link #parentWindowId} will not be null) and it has a relative
 * position to it.
 * @author Markus
 */
public class WindowSettings implements Serializable {

	private String windowId;
	private String title;
	private int width;
	private int height;

	/** Is it a popup window? Will be null if not. */
	private String parentWindowId;

	/**
	 * Position relative to its parent. Value might be negative. Must be set if the window
	 * is a child.
	 */
	private int positionX;
	private int positionY;

	public WindowSettings() {
		// default constructor
	}

	public WindowSettings(String windowId, String title, int width, int height, String parentWindowId, int positionX, int positionY) {
		this.windowId = windowId;
		this.title = title;
		this.width = width;
		this.height = height;
		this.parentWindowId = parentWindowId;
		this.positionX = positionX;
		this.positionY = positionY;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[windowId=" + windowId + ", title=" + title + ", width=" + width + ", height=" + height + ", parentWindowId=" + parentWindowId + ", positionX=" + positionX
				+ ", positionY=" + positionY + "]";
	}

	public String getWindowId() {
		return windowId;
	}

	public void setWindowId(String windowId) {
		this.windowId = windowId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getParentWindowId() {
		return parentWindowId;
	}

	public void setParentWindowId(String parentWindowId) {
		this.parentWindowId = parentWindowId;
	}

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

}
