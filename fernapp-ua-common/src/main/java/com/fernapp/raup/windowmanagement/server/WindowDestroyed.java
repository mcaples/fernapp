package com.fernapp.raup.windowmanagement.server;

import java.io.Serializable;


/**
 * Informs the client that a window is permanently gone. Messages are delivered in causal
 * order. Meaning on window deletion, delete messages for child windows will be delivered
 * before the message for the parent.
 * @author Markus
 */
public class WindowDestroyed implements Serializable {

	private String windowId;

	public WindowDestroyed() {
		// default constructor
	}

	public WindowDestroyed(String windowId) {
		this.windowId = windowId;
	}

	public String getWindowId() {
		return windowId;
	}

	public void setWindowId(String windowId) {
		this.windowId = windowId;
	}

}
