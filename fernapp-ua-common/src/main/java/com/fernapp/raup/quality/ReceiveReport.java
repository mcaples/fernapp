package com.fernapp.raup.quality;

import java.io.Serializable;

import com.fernapp.raup.windowmanagement.server.WindowContentUpdate;


/**
 * Receive report for a {@link WindowContentUpdate}. The receive report must include the
 * time obtained by {@link WindowContentUpdate#getServerSendTime()}. Is used for QoS
 * detection on the server-side and adaptive encoding of the video stream.
 * @author Markus
 */
public class ReceiveReport implements Serializable {

	private long serverSendTime;

	public ReceiveReport() {
		// default constructor
	}

	public ReceiveReport(long serverSendTime) {
		this.serverSendTime = serverSendTime;
	}

	public long getServerSendTime() {
		return serverSendTime;
	}

	public void setServerSendTime(long serverSendTime) {
		this.serverSendTime = serverSendTime;
	}

}
