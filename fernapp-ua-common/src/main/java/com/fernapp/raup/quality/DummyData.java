package com.fernapp.raup.quality;

import java.io.Serializable;


/**
 * Dummy data that may be send of the connection. The receiver must ignore these messages.
 * @author Markus
 */
public class DummyData implements Serializable {

	private byte[] payload;

	public DummyData() {
		// default constructor
	}

	/**
	 * @param payload
	 */
	public DummyData(byte[] payload) {
		this.payload = payload;
	}

	/**
	 * @return the payload
	 */
	public byte[] getPayload() {
		return payload;
	}

	/**
	 * @param payload the payload to set
	 */
	public void setPayload(byte[] payload) {
		this.payload = payload;
	}

}
