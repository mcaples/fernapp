package com.fernapp.raup.measurement;

import java.io.Serializable;

import com.fernapp.uacommon.measurement.Measurement;


/**
 * The client can send {@link Measurement}s made on the client machine to the server for
 * logging.
 * @author Markus
 */
public class RemoteMeasurements implements Serializable {

	private Measurement measurement;

	public RemoteMeasurements() {
		// default constructor
	}

	public RemoteMeasurements(Measurement measurement) {
		this.measurement = measurement;
	}

	public Measurement getMeasurement() {
		return measurement;
	}

}
