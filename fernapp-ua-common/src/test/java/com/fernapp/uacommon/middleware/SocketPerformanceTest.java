package com.fernapp.uacommon.middleware;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import junit.framework.Assert;

import org.junit.Test;

import com.fernapp.uacommon.middleware.channel.SocketFactory;
import com.google.common.base.Stopwatch;


/**
 * For socket/TCP analysis.
 * 
 * tcpdump -n -i lo port 1234
 * 
 * @author Markus
 */
public class SocketPerformanceTest {

	@Test
	public void testTcpSlowStart() throws Exception {
		// open socket connection
		final int port = 1234;
		final ServerSocket ss = new ServerSocket();
		ss.bind(new InetSocketAddress(port));
		final Socket clientSocket = SocketFactory.createClientSocket();
		Thread clientConnectThread = new Thread(new Runnable() {
			public void run() {
				try {
					clientSocket.connect(new InetSocketAddress("localhost", port), 3000);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		clientConnectThread.start();
		final Socket serverSocket = ss.accept();
		clientConnectThread.join();
		SocketFactory.configureServerSocket(serverSocket);
		System.out.println("server getSendBufferSize: " + serverSocket.getSendBufferSize());
		System.out.println("client getReceiveBufferSize: " + clientSocket.getReceiveBufferSize());

		try {
			for (int i = 0; i < 10; i++) {
				final int dataLength = 50000;
				final Stopwatch stopwatch = new Stopwatch();

				// prepare receiver
				Thread receiveThread = new Thread(new Runnable() {
					public void run() {
						try {
							DataInput din = new DataInputStream(clientSocket.getInputStream());
							din.readFully(new byte[dataLength], 0, dataLength);
							stopwatch.stop();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				});
				receiveThread.start();

				// Thread.sleep(10000);

				// send big chunk
				stopwatch.start();
				serverSocket.getOutputStream().write(new byte[dataLength]);
				serverSocket.getOutputStream().flush();

				// verify
				receiveThread.join();
				Assert.assertTrue(!stopwatch.isRunning());
				System.out.println("time: " + stopwatch.elapsedMillis());
			}
		} finally {
			serverSocket.close();
			clientSocket.close();
			ss.close();
		}
	}

}
